== Lista de comida

// Ordenada por orden alfabético en cada apartado

=== Aperitivos

* Campesinas
* Doritos Chili
* Patatas fritas
* Huesos de aceituna
* Ferrero rocher

=== Platos principales

* Chuletas de cabeza
* Panceta de cerdo
* Filetes de pollo
* Paella

=== Postres

* Arroz con leche
* Tarta de queso
